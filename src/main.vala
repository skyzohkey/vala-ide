using ValaIDE;
using ValaIDE.Core;
//using ValaIDE.Utils;
//using ValaIDE.GUI;

namespace ValaIDE {
  public static int main (string[] args) {
    /**
    * Let's start our amazing IDE. :)
    * @NOTE: This place may also handle plugins loading in a not so far future.
    **/

    Application app = new Application ();
    app.parse_args (ref args);
    app.run ();

    return 0;
  }
}
