using ValaIDE;
using ValaIDE.Utils;

namespace ValaIDE.Core {
  public class Application : Gtk.ApplicationWindow, Object {
    /**
    * Let's define our OptionContext entries.
    **/
    private static bool version = false;
    private static string project_path = null;
    private const GLib.OptionEntry[] options = {
      { "version", "v", 0, OptionArg.NONE, ref version, "Displays Vala IDE version.", null },
      { "project", "p", 0, OptionArg.STRING, ref project_path, "Path to the project (.vproj) to open.", "PATH" },
      { null } // List terminator.
    };

    /**
    * The loaded project. Can be null.
    **/
    private Project? project { get; internal set; default = null; }

    /**
    * The application constructor.
    **/
    public Application () {
      /**
      * TODO: Init variables/properties.
      * TODO: Init GUI.
      * TODO: Connect signals.
      * TODO: Connect callbacks.
      **/
    }

    /**
    * This method permits to parse arguments and handle them properly.
    * @param {string[]} args - A reference from the app entrypoint.
    **/
    public void parse_args (ref string[] args) {
      try {
        var opt_context = new OptionContext ("- Vala IDE: Simple but elegant Vala code editor.");
        opt_context.set_help_enabled (true);
        opt_context.add_main_entries (this.options, null);
        opt_context.parse (ref args);
      } catch (OptionError e) {
        stdout.printf ("error: %s\n", e.message);
        stdout.printf ("Run '%s --help' to see a full list of available command line options.\n", args[0]);
        return 0;
      }

      if (version) {
        print ("%s version %s", Constants.APP_NAME, Constants.APP_VERSION);
        return 0;
      }

      if (project_path != null) {
        /**
        * TODO: Check that file exists, if not, start a blank project. Else:
        * TODO: Call `this.load_project (this.project_path);`
        **/
      }
    }

    /**
    * This method handle all the Gtk initialization, projects loading, etc.
    **/
    public void run () {
      print ("%s version %s starting...", Constants.APP_NAME, Constants.APP_VERSION);

      /**
      * TODO: Once GUI is loaded, let's load the last opened projects/files.
      **/
    }
  }
}
