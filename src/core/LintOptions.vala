using ValaIDE;

namespace ValaIDE.Core {
  public struct LintOptions {
    public bool enabled;
    public bool spaces_before_parenthesis;
    public bool no_as_keyword;
    public bool capitalized_consts;
    public bool one_object_per_file;
    public bool run_astyle;
  }
}
