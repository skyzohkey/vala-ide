using ValaIDE;
using ValaIDE.Utils;

namespace ValaIDE.Core {
  public class Project : Object {
    public string path { get; set; default = ""; }
    public string name { get; set; default = ""; }
    public string description { get; set; default = ""; }
    public string author { get; set; default = ""; }
    public string version { get; set; default = "0.1.0"; }
    public string[]? packages { get; set; default = null; }
    public string[]? sources { get; set; default = null; }
    public ValaFlags? flags { get; set; default = null; }
    public LintOptions? lint { get; set; default = null; }

    public Project (string path) throws Error {
      this.path = path;

      if (FileUtils.test (this.path, FileTest.EXISTS)) {
        this.load ();
      } else {
        throw new Error (@"File $(this.path) doesn't exists. Cannot load the project.");
      }
    }

    private void load () {
      /**
      * TODO: Read the .vproj file and deserialize it.
      * TODO: Call this.check_packages, this.check_sources and this.check_flags.
      * TODO: Check if linting is enabled, if so apply it.
      **/
    }
  }
}
