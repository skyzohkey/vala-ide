# Vala IDE - Simple and elegant Vala code editor.

Vala IDE is a personal attempt to make something lightweight but powerful that
allows me to write Vala code way better.

I'd like to add the following features (list is not hierarchized) :

- [ ] Syntax highlighting ;
- [ ] Code completion ;
- [ ] Live code debug/infos ;
- [ ] Built-in linting (errors, astyle, etc) ;
- [ ] Symbols tree ;
- [ ] Project files tree ;
- [ ] Packages/dependencies management ;
- [ ] Resources management : in order to handle gresources more easily ;
- [ ] Languages management : to allow easy language creation/modifications ;
- [ ] Simple way to launch debug/release (F5) + easy way to set a build command ;
- [ ] Glade integration : to handle .ui files easier ;
- [ ] Maybe a plugins system, with libpeas.
